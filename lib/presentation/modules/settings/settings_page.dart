import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watermark_for_image/presentation/modules/settings/bloc/settings_bloc.dart';
import 'package:watermark_for_image/presentation/modules/settings/bloc/settings_event.dart';
import 'package:watermark_for_image/presentation/modules/settings/bloc/settings_state.dart';

import 'widgets/drop_down_list.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return SettingsBloc();
      },
      child: const SettingsPageView(),
    );
  }
}

class SettingsPageView extends StatefulWidget {
  const SettingsPageView({Key? key}) : super(key: key);

  @override
  State<SettingsPageView> createState() => _SettingsPageViewState();
}

class _SettingsPageViewState extends State<SettingsPageView> {
  @override
  void initState() {
    context.read<SettingsBloc>().add(LoadingSettingsEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Настройки'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 8.0),
        child: BlocConsumer<SettingsBloc, SettingsState>(
          builder: (context, state) => state is InitialSettingsState
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView(
                  children: [
                    Container(
                      color: Colors.black12,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Column(
                          children: const [
                            SizedBox(height: 10.0),
                            Text(
                              'Возянной знак, который будет наложен на загружаемые изображения',
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 10.0),
                            SuperimposedWatermarkImage(),
                            SizedBox(height: 10.0),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    Container(
                      color: Colors.black12,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Column(
                          children: const [
                            SizedBox(height: 10.0),
                            Text(
                              'Настройки сжатия изображения',
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 10.0),
                            SettingsImageCompression(),
                            SizedBox(height: 10.0),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    Container(
                      color: Colors.black12,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Column(
                          children: const [
                            SizedBox(height: 10.0),
                            Text(
                              'Настройка размера загружаемого изображения',
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 10.0),
                            _SettingsSizeImage(),
                            SizedBox(height: 10.0),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    //Text(state.repository.getImageWatermark().toString()),
                  ],
                ),
          buildWhen: (previous, current) {
            return true;
          },
          listener: (context, state) {
            if (state is ErrorSettingsState) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.errorText),
                ),
              );
            }
          },
          listenWhen: (previous, current) {
            return current is ErrorSettingsState;
          },
        ),
      ),
    );
  }
}

class SuperimposedWatermarkImage extends StatelessWidget {
  const SuperimposedWatermarkImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, state) => Row(
        children: [
          state is EditSettingsState
              ? SizedBox(
                  width: MediaQuery.of(context).size.width / 3,
                  child: Image.asset(
                    state.settings.imageWatermark,
                  ),
                )
              : Container(),
          Expanded(
            child: Column(
              children: [
                const Text(
                  '*Изображение по умолчанию',
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  onPressed: () {},
                  child: const Text('Изменить'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class SettingsImageCompression extends StatefulWidget {
  const SettingsImageCompression({Key? key}) : super(key: key);

  @override
  State<SettingsImageCompression> createState() =>
      _SettingsImageCompressionState();
}

class _SettingsImageCompressionState extends State<SettingsImageCompression> {
  final List<String> listCompressionRatioImage = [
    "30",
    "50",
    "70",
    "90",
    "100"
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, state) => state is EditSettingsState
          ? Column(
              children: [
                Row(
                  children: [
                    const Text(
                      "Сжать изображение",
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Checkbox(
                      checkColor: Colors.white,
                      value: state.settings.compressOriginalImage,
                      onChanged: (bool? value) {
                        if (value != null) {
                          context.read<SettingsBloc>().add(
                                EditSettingsEvent(
                                  newValueCompressOriginalImage: value,
                                  settings: state.settings,
                                ),
                              );
                        }
                      },
                    ),
                  ],
                ),
                if (state.settings.compressOriginalImage)
                  Row(
                    children: [
                      const Expanded(
                        child: Text(
                          "Коэффицент сжатия изображаения",
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10.0,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 4,
                        child: DropDownList(
                          list: listCompressionRatioImage,
                          compress: state.settings.compressionRatioImage,
                          signature: '%',
                          onPressed: (int index) {
                            context.read<SettingsBloc>().add(
                                  EditSettingsEvent(
                                    newCompressionRatioImage: int.parse(
                                        listCompressionRatioImage[index]),
                                    settings: state.settings,
                                  ),
                                );
                          },
                        ),
                      ),
                    ],
                  )
                else
                  Container(),
              ],
            )
          : Container(),
    );
  }
}

class _SettingsSizeImage extends StatefulWidget {
  const _SettingsSizeImage({Key? key}) : super(key: key);

  @override
  State<_SettingsSizeImage> createState() => _SettingsSizeImageState();
}

class _SettingsSizeImageState extends State<_SettingsSizeImage> {
  final List<String> listMaxSizeOriginalImage = ["35", "50", "70", "80", "100"];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, state) => state is EditSettingsState
          ? Row(
              children: [
                const Expanded(
                  child: Text(
                    "Максимальный размер загружаемого изображения",
                    style: TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 4,
                  child: DropDownList(
                    list: listMaxSizeOriginalImage,
                    compress: state.settings.maxSizeOriginalImage,
                    signature: ' мб',
                    onPressed: (int index) {
                      context.read<SettingsBloc>().add(
                            EditSettingsEvent(
                              newMaxSizeOriginalImage:
                                  int.parse(listMaxSizeOriginalImage[index]),
                              settings: state.settings,
                            ),
                          );
                    },
                  ),
                ),
              ],
            )
          : Container(),
    );
  }
}
