import 'package:watermark_for_image/data/models/settings.dart';
//состояния экрана

abstract class SettingsState {}

class InitialSettingsState extends SettingsState {}

class EditSettingsState extends SettingsState {
  final Settings settings;

  EditSettingsState({required this.settings});
}

class ErrorSettingsState extends SettingsState {
  final String errorText;

  ErrorSettingsState(
    this.errorText,
  ) : super();
}

