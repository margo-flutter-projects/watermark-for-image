//событие
import 'package:watermark_for_image/data/models/settings.dart';

abstract class SettingsEvent {}

class EditSettingsEvent extends SettingsEvent {
  final bool? newValueCompressOriginalImage;
  final int? newCompressionRatioImage;
  final int? newMaxSizeOriginalImage;
  final Settings settings;

  EditSettingsEvent({
    this.newValueCompressOriginalImage,
    this.newCompressionRatioImage,
    this.newMaxSizeOriginalImage,
    required this.settings,
  });
}

class LoadingSettingsEvent extends SettingsEvent {}
