import 'package:bloc/bloc.dart';
import 'package:watermark_for_image/data/models/repository.dart';
import 'package:watermark_for_image/data/models/settings.dart';
import 'package:watermark_for_image/presentation/modules/settings/bloc/settings_event.dart';
import 'package:watermark_for_image/presentation/modules/settings/bloc/settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final repository = Repository();

  SettingsBloc() : super(InitialSettingsState()) {
    on<LoadingSettingsEvent>((event, emit) async {
      emit(InitialSettingsState());
      try {
        String imageWatermark = await repository.getImageWatermark();
        bool compressOriginalImage =
            await repository.getCompressOriginalImage();
        int compressionRatioImage = await repository.getCompressionRatioImage();
        int maxSizeOriginalImage = await repository.getMaxSizeOriginalImage();
        emit(
          EditSettingsState(
            settings: Settings(
              imageWatermark: imageWatermark,
              compressOriginalImage: compressOriginalImage,
              compressionRatioImage: compressionRatioImage,
              maxSizeOriginalImage: maxSizeOriginalImage,
            ),
          ),
        );
      } catch (e) {
        emit(InitialSettingsState());
      }
    });

    on<EditSettingsEvent>((event, emit) async {
      Settings _settings = event.settings;
      String imageWatermark = _settings.imageWatermark;
      bool compressOriginalImage = _settings.compressOriginalImage;
      int compressionRatioImage = _settings.compressionRatioImage;
      int maxSizeOriginalImage = _settings.maxSizeOriginalImage;
      try {
        if (event.newValueCompressOriginalImage != null) {
          compressOriginalImage = event.newValueCompressOriginalImage!;
          repository.setCompressOriginalImage(compressOriginalImage);
        }

        if (event.newCompressionRatioImage != null) {
          compressionRatioImage = event.newCompressionRatioImage!;
          repository.setCompressionRatioImage(compressionRatioImage);
        }

        if (event.newMaxSizeOriginalImage != null) {
          maxSizeOriginalImage = event.newMaxSizeOriginalImage!;
          repository.setMaxSizeOriginalImage(maxSizeOriginalImage);
        }

        emit(
          EditSettingsState(
            settings: Settings(
              imageWatermark: imageWatermark,
              compressOriginalImage: compressOriginalImage,
              compressionRatioImage: compressionRatioImage,
              maxSizeOriginalImage: maxSizeOriginalImage,
            ),
          ),
        );
      } catch (e) {
        emit(InitialSettingsState());
      }
    });
  }
}
