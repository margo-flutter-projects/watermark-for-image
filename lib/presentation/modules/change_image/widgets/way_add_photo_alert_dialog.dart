import 'package:flutter/material.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_event.dart';

class WayAddPhotoAlertDialog extends StatefulWidget {
  const WayAddPhotoAlertDialog({Key? key}) : super(key: key);

  @override
  State<WayAddPhotoAlertDialog> createState() {
    return WayAddPhotoAlertDialogState();
  }
}

class WayAddPhotoAlertDialogState extends State<WayAddPhotoAlertDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        "Выберете способ добавления изображения",
        style: TextStyle(color: Colors.blue),
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            const Divider(
              height: 1,
              color: Colors.blue,
            ),
            _MyListTile(
              iconData: Icons.account_box,
              text: "Галерея",
              onPressed: () {
                Navigator.pop(context, AddPhotosFromGalleryEvent());
              },
            ),
            const Divider(
              height: 1,
              color: Colors.blue,
            ),
            const Divider(
              height: 1,
              color: Colors.blue,
            ),
            _MyListTile(
              iconData: Icons.camera,
              text: "Сделать фото",
              onPressed: () {
                Navigator.pop(context, AddPhotosFromCameraEvent());
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _MyListTile extends StatefulWidget {
  final VoidCallback? onPressed;
  final IconData iconData;
  final String text;

  const _MyListTile({
    Key? key,
    this.onPressed,
    required this.iconData,
    required this.text,
  }) : super(key: key);

  @override
  State<_MyListTile> createState() => _MyListTileState();
}

class _MyListTileState extends State<_MyListTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: widget.onPressed,
      title: Text(widget.text),
      leading: Icon(
        widget.iconData,
        color: Colors.blue,
      ),
    );
  }
}
