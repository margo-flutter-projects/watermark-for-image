abstract class ChangeImageEvent {}

class AddPhotosFromGalleryEvent extends ChangeImageEvent {}

class AddPhotosFromCameraEvent extends ChangeImageEvent {}

class DeletePhoto extends ChangeImageEvent {
  final String id;

  DeletePhoto(this.id);
}
