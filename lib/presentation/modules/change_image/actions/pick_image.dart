import 'package:image_picker/image_picker.dart';

abstract class AddImagePicker {
  Future<List<XFile>?> pickMultiImage();

  Future<XFile?> pickImage();
}

class PickImages implements AddImagePicker {
  @override
  Future<List<XFile>?> pickMultiImage() async {
    final _imagePicker = ImagePicker();
    final List<XFile>? images = await _imagePicker.pickMultiImage();
    return images;
  }

  @override
  Future<XFile?> pickImage() async {
    final _imagePicker = ImagePicker();
    final XFile? photo =
        await _imagePicker.pickImage(source: ImageSource.camera);
    return photo;
  }
}
