import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:watermark_for_image/data/models/image_add.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_bloc.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_event.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_state.dart';
import 'package:watermark_for_image/presentation/modules/change_image/widgets/way_add_photo_alert_dialog.dart';
import 'package:watermark_for_image/presentation/modules/photo_view/photo_view_page.dart';
import 'package:watermark_for_image/presentation/modules/settings/settings_page.dart';

class ChangeImagePage extends StatelessWidget {
  const ChangeImagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return ChangeImageBloc();
      },
      child: const ChangeImagePageView(),
    );
  }
}

class ChangeImagePageView extends StatelessWidget {
  const ChangeImagePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Наложить водяной знак'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const SettingsPage(),
                ),
              );
            },
            icon: const Icon(Icons.settings),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          children: [
            Expanded(
              child: _AddWatermarkOnImage(),
            ),
            _DownloadNewImage(),
          ],
        ),
      ),
    );
  }
}

class _AddWatermarkOnImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeImageBloc, ChangeImageState>(
      builder: (context, state) => GridView.count(
        crossAxisCount: 2,
        padding: const EdgeInsets.all(8),
        children:
            List<Widget>.generate(state.listImagesAdd.length + 1, (index) {
          if (index < state.listImagesAdd.length) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 2.0, vertical: 4.0),
              child: state.listImagesAdd[index].percentLoading == 100 &&
                      state.listImagesAdd[index].imageTransform != null
                  ? _Image(imageAdd: state.listImagesAdd[index])
                  : LoadingImage(state.listImagesAdd[index].percentLoading),
            );
          } else {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 2.0, vertical: 4.0),
              child: _AddImageButton(),
            );
          }
        }),
      ),
    );
  }
}

class _DownloadNewImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      child: const Text(
        'Скачать все изображения',
        style: TextStyle(fontSize: 16.0),
      ),
    );
  }
}

class _AddImageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: Center(
        child: IconButton(
          onPressed: () async {
            final result = await showDialog(
              context: context,
              builder: (BuildContext context) => const WayAddPhotoAlertDialog(),
            );
            if (result != null) context.read<ChangeImageBloc>().add(result);
            //context.read().add(result);
          },
          icon: const Icon(
            Icons.add,
            color: Colors.grey,
            size: 40.0,
          ),
        ),
      ),
    );
  }
}

class _Image extends StatelessWidget {
  final ImageAdd imageAdd;

  const _Image({
    Key? key,
    required this.imageAdd,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          child: ListTile(
            leading: Hero(
              tag: imageAdd.id.toString(),
              child: Image.memory(
                imageAdd.imageTransform!,
                fit: BoxFit.cover,
              ),
            ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (BuildContext context) => PhotoViewPage(
                  imageAdd.id.toString(),
                  imageAdd.imageTransform!,
                ),
              ));
            },
          ),
        ),
        Positioned(
          top: 0.0,
          right: 0.0,
          child: IconButton(
            icon: const Icon(
              Icons.cancel,
              color: Colors.black54,
              size: 30.0,
            ),
            onPressed: () =>
                context.read<ChangeImageBloc>().add(DeletePhoto(imageAdd.id)),
          ),
        ),
      ],
    );
  }
}

class LoadingImage extends StatelessWidget {
  final double percentLoading;

  const LoadingImage(
    this.percentLoading, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.black12,
        ),
        Center(
          child: CircularPercentIndicator(
            radius: 50.0,
            lineWidth: 6.0,
            percent: percentLoading / 100,
            animation: true,
            center: Text(
              percentLoading.toInt().toString() + '%',
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 19.0,
              ),
            ),
            circularStrokeCap: CircularStrokeCap.round,
            backgroundColor: Colors.white24,
            progressColor: Colors.white,
          ),
        ),
      ],
    );
  }
}
