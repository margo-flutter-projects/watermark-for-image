import 'package:shared_preferences/shared_preferences.dart';

class Repository {
  Future<String> getImageWatermark() async {
    final prefs = await SharedPreferences.getInstance();
    String? imageWatermark = prefs.getString('path_image_watermark');
    if (imageWatermark == null) {
      return 'images/watermark.png';
    } else {
      return imageWatermark;
    }
  }

  Future<bool> getCompressOriginalImage() async {
    final prefs = await SharedPreferences.getInstance();
    bool? isCompress = prefs.getBool('is_compress');
    if (isCompress == null) {
      return true;
    } else {
      return isCompress;
    }
  }

  /*
  коэффицент сжатия изображения
  */
  Future<int> getCompressionRatioImage() async {
    final prefs = await SharedPreferences.getInstance();
    int? cri = prefs.getInt('compression_ratio_image');
    if (cri == null) {
      return 90;
    } else {
      return cri;
    }
  }

  /*в мбитах*/
  Future<int> getMaxSizeOriginalImage() async {
    final prefs = await SharedPreferences.getInstance();
    int? maxsize = prefs.getInt('max_size_original');
    if (maxsize == null) {
      return 50;
    } else {
      return maxsize;
    }
  }

  void setImageWatermark(String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('path_image_watermark', value);
  }

  void setCompressOriginalImage(bool value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_compress', value);
  }

  void setCompressionRatioImage(int value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('compression_ratio_image', value);
  }

  /*в битах*/
  void setMaxSizeOriginalImage(int value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('max_size_original', value);
  }
}
